package pt.ua.tqs.tqskeychain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Random;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * A text-oriented keychain to keep a list of passwords in a file
 *
 * @author tqs student
 */
public class TqsKeychain {

    private static final String KEYCHAINCIFRA = "Keychain-cifrado.txt";
    private static final String KEY = "MySEcRetKeY";
    private static final String ALGORITHM = "AES";
    private static final String PROVIDER = "SunJCE";
    private static final String PARAMETERS = "AES/ECB/PKCS5Padding";
    private static final String KEYCHAINFILE = "Keychain.txt";
    private static final Scanner s = new Scanner(System.in);

    private static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private TqsKeychain() {

    }

    public static void novaEntrada(File keys) {

        log.log(Level.INFO, "OPÇÃO 1");
        log.log(Level.INFO, "Nome da aplicação/site? ");
        String platform = s.next();
        StringBuilder sb = new StringBuilder(platform);
        sb.append(",");
        log.log(Level.INFO, "Utilizador? ");
        String user = s.next();
        sb.append(user);
        sb.append(",");
        log.log(Level.INFO, "Gerar password? (y/n)");
        String yn = s.next();
        if ("y".equals(yn) || "Y".equals(yn)) {
            String generatedPass = nextSessionPass();
            log.log(Level.INFO, "Nova Pass > " + generatedPass);
            sb.append(generatedPass);
        } else if ("n".equals(yn) || "N".equals(yn)) {
            log.log(Level.INFO, "Digite Pass");
            String pass = s.next();
            sb.append(pass);
        } else {
            log.log(Level.INFO, "opcao incorrecta");
        }

        FileWriter fw = null;
        try {
            fw = new FileWriter(keys, true);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(TqsKeychain.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedWriter bw = new BufferedWriter(fw);
        try {
            bw.write(sb.toString());
        } catch (IOException ex) {
            Logger.getLogger(TqsKeychain.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw.newLine();
        } catch (IOException ex) {
            Logger.getLogger(TqsKeychain.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(TqsKeychain.class.getName()).log(Level.SEVERE, null, ex);
        }

        log.log(Level.INFO, "Utilizador guardado.");
        log.log(Level.INFO, "\n");
    }

    public static void listarFicheiro(Scanner scFile) {

        log.log(Level.INFO, "Aplicação       User          Password");
        log.log(Level.INFO, "-------------------------------------");
        while (scFile.hasNextLine()) {
            String entry = scFile.nextLine();
            String[] words = entry.trim().split(",");
            log.log(Level.INFO, String.format("%-10s  %8s  %15s", words[0], words[1], words[2]));
            log.log(Level.INFO, "\n");

        }
        scFile.close();
    }

    public static void credenciais(Scanner scFile) {
        log.log(Level.INFO, "OPCAO 3");
        log.log(Level.INFO, "Aplicação a procurar? ");
        String search = s.next();
        String[] palavras;
        log.log(Level.INFO, "Platform       User          Password");
        log.log(Level.INFO, "-------------------------------------");

        while (scFile.hasNextLine()) {
            palavras = scFile.nextLine().split(",");
            if (palavras[0].startsWith(search)) {
                log.log(Level.INFO, String.format("%-10s  %8s  %15s", palavras[0], palavras[1], palavras[2]));
                log.log(Level.INFO, "\n");
            }

        }

        scFile.close();

        log.log(Level.INFO, "\n");
    }

    public static String construirChave(String key) {
        String keyTemp = key;
        int length = key.length();
        if (length > 16 && length != 16) {
            keyTemp = key.substring(0, 15);
        }
        if (length < 16 && length != 16) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 16 - length; i++) {
                keyTemp = sb.append(new StringBuilder(key).append("0").toString()).toString();
            }
        }
        return keyTemp;
    }

    public static void openTXTFile(Cipher encrypt) {
        try {
            FileInputStream fis = new FileInputStream(KEYCHAINFILE);
            fis.close();
            CipherInputStream cis = new CipherInputStream(fis, encrypt);
            cis.close();
            // Write to the Encrypted file  
            FileOutputStream fos = new FileOutputStream(KEYCHAINCIFRA);
            fos.close();
            byte[] b = new byte[8];
            int i = cis.read(b);
            while (i != -1) {
                fos.write(b, 0, i);
                i = cis.read(b);
            }

        } catch (IOException err) {
            log.log(Level.INFO, (Supplier<String>) err);
        }
    }

    public static void cifrarFicheiro() {
        log.log(Level.INFO, "OPCAO 4");
        try {
            //Creation of Secret key  
            String key = KEY;

            // AES needs exaxtly 16 chars
            key = construirChave(key);

            SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            //Creation of Cipher objects  
            Cipher encrypt = Cipher.getInstance(PARAMETERS, PROVIDER);
            encrypt.init(Cipher.ENCRYPT_MODE, secretKey);
            // Open the Plaintext file  
            openTXTFile(encrypt);
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
            log.info((Supplier<String>) e);
        }
        log.log(Level.INFO, "Ficheiro Cifrado em Keychain-cifrado.txt");
        log.log(Level.INFO, "\n");
    }

    public static void decifrarFicheiro() {
        log.log(Level.INFO, "OPCAO 5");
        try {
            File aesFile = new File(KEYCHAINCIFRA);
            File aesFileBis = new File(KEYCHAINFILE);
            FileInputStream fis;
            FileOutputStream fos;
            CipherInputStream cis;
            //Creation of Secret key  
            String key = KEY;

            // AES needs exaxtly 16 chars
            key = construirChave(key);

            SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            //Creation of Cipher objects  
            Cipher decrypt = Cipher.getInstance(PARAMETERS, PROVIDER);

            decrypt.init(Cipher.DECRYPT_MODE, secretKey);
            // Open the Encrypted file  
            fis = new FileInputStream(aesFile);
            fis.close();
            cis = new CipherInputStream(fis, decrypt);
            // Write to the Decrypted file  
            fos = new FileOutputStream(aesFileBis);
            fos.close();
            byte[] b = new byte[8];
            int i = cis.read(b);
            while (i != -1) {
                fos.write(b, 0, i);
                i = cis.read(b);
            }
            cis.close();
        } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
            log.info((Supplier<String>) e);
        }
        log.log(Level.INFO, "Decifrado");
        log.log(Level.INFO, "\n");
    }

    public static void main(String[] args) throws FileNotFoundException {

        File keys = new File(KEYCHAINFILE);
        Scanner scFile;
        int op;
        log.log(Level.INFO, "\n1 - Nova entrada \n2 - Listar keychain \n3 - Pesquisar credenciais p/ aplicação\n"
                + "4 - Cifrar Ficheiro \n5 - Decifrar Ficheiro \n0 - Sair \n");

        op = s.nextInt();
        log.log(Level.FINE, "\n");

        do {
            switch (op) 
            {
                case 0:
                    try {
                        log.log(Level.INFO, "Sair do programa");
                    } catch (NullPointerException npE) {
                        log.info((Supplier<String>) npE);
                    } break;
                case 1:
                    // escolheu adicionar nova entrada
                    novaEntrada(keys);
                    break;

                case 2:
                    scFile = new Scanner(keys);
                    listarFicheiro(scFile);
                    break;

                case 3:
                    //pesquisar credenciais pelo nome da aplicação
                    scFile = new Scanner(keys);
                    credenciais(scFile);
                    break;

                case 4://cifrar ficheiro
                    cifrarFicheiro();
                    break;

                case 5:
                    decifrarFicheiro();
                    break;

                default:
                    try {
                        log.log(Level.INFO, "Código inválido. Tente de novo. \n");
                    } catch (NullPointerException npE) {
                        log.info((Supplier<String>) npE);
                    }
            }
        } while (op != 0);

    }

    // Criar uma password complexa Alphanumerica
    public static String nextSessionPass() {
        Random rand = new Random();
        return new BigInteger(90, rand).toString(32);
    }
}
